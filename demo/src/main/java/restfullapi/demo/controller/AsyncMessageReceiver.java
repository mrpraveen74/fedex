package restfullapi.demo.controller;

import java.util.concurrent.TimeUnit;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageListener;
import javax.jms.Session;

import com.amazon.sqs.javamessaging.ProviderConfiguration;
import com.amazon.sqs.javamessaging.SQSConnection;
import com.amazon.sqs.javamessaging.SQSConnectionFactory;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.sqs.AmazonSQSClient;
import com.amazonaws.services.sqs.AmazonSQSClientBuilder;
import com.amazonaws.services.sqs.model.GetQueueUrlRequest;
import com.amazonaws.services.sqs.model.GetQueueUrlResult;

public class AsyncMessageReceiver {
    @SuppressWarnings("deprecation")
	public static void main(String args[]) throws JMSException, InterruptedException {
  
    	AWSCredentials credentials = new BasicAWSCredentials(
    			  "AKIAQHMVH22J3FOOOMWI", 
    			  "CBGaAohk8N0uRNfmV3lnwHVp0XexwPQ5nzg+Vkld"
    			);
    	
    	    	    
    	SQSConnectionFactory connectionFactory = new SQSConnectionFactory(
                new ProviderConfiguration(),
                AmazonSQSClientBuilder.standard()
                        .withRegion(Regions.AP_SOUTH_1)
                        .withCredentials(new AWSStaticCredentialsProvider(credentials))
                );
         
    	
    	
    	/*AmazonSQSClient awsClient = new AmazonSQSClient(credentials);
    	awsClient.setEndpoint("sqs.ap-south-1.amazonaws.com");
    	awsClient.withRegion(Regions.AP_SOUTH_1);
    	// get the Q url
    	GetQueueUrlResult result = awsClient.getQueueUrl(new GetQueueUrlRequest("TaxQueue"));*/
    			
        // Create the connection
        SQSConnection connection = connectionFactory.createConnection();
         
       // Create the session
        Session session = connection.createSession(false, Session.CLIENT_ACKNOWLEDGE);
        MessageConsumer consumer = session.createConsumer( session.createQueue("TaxQueue"));
         
        ReceiverCallback callback = new ReceiverCallback();
        consumer.setMessageListener( callback );

        // No messages are processed until this is called
        connection.start();
         
        callback.waitForOneMinuteOfSilence();
        System.out.println( "Returning after one minute of silence" );

        // Close the connection. This closes the session automatically
        connection.close();
        System.out.println( "Connection closed" );
    }
    
    
    private static class ReceiverCallback implements MessageListener {
        // Used to listen for message silence
        private volatile long timeOfLastMessage = System.nanoTime();
         
        public void waitForOneMinuteOfSilence() throws InterruptedException {
            for(;;) {
                long timeSinceLastMessage = System.nanoTime() - timeOfLastMessage;
                long remainingTillOneMinuteOfSilence = 
                    TimeUnit.MINUTES.toNanos(1) - timeSinceLastMessage;
                if( remainingTillOneMinuteOfSilence < 0 ) {
                    break;
                }
                TimeUnit.NANOSECONDS.sleep(remainingTillOneMinuteOfSilence);
            }
        }
         

        @Override
        public void onMessage(Message message) {
            System.out.println( "Acknowledged message " + message.toString() );
			timeOfLastMessage = System.nanoTime();
        }
    }
}
